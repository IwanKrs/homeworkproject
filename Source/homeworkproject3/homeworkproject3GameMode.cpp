// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "homeworkproject3GameMode.h"
#include "homeworkproject3HUD.h"
#include "homeworkproject3Character.h"
#include "UObject/ConstructorHelpers.h"

Ahomeworkproject3GameMode::Ahomeworkproject3GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = Ahomeworkproject3HUD::StaticClass();
}
