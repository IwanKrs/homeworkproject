// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "homeworkproject3HUD.generated.h"

UCLASS()
class Ahomeworkproject3HUD : public AHUD
{
	GENERATED_BODY()

public:
	Ahomeworkproject3HUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

