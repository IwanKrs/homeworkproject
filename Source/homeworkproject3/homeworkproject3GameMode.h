// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "homeworkproject3GameMode.generated.h"

UCLASS(minimalapi)
class Ahomeworkproject3GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Ahomeworkproject3GameMode();
};



